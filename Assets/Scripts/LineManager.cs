﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineManager : MonoBehaviour {

    public Transform ball;
    public string hand;
    public Transform otherObj;

    private LineRenderer line;

	// Use this for initialization
	void Start () {
        line = GetComponent<LineRenderer>();

        if (GameObject.Find(hand) != null)
            otherObj = GameObject.Find(hand).transform;

    }
	
	// Update is called once per frame
	void Update () {
        if (otherObj)
        {
            line.SetPosition(0, ball.position);
            line.SetPosition(1, otherObj.position);

        }

    }
}
