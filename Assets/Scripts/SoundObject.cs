﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundObject : MonoBehaviour
{
    public bool restart;
    GameController gc;
    AudioSource src;
    bool isColliding;
    //public string target;

	// Use this for initialization
	void Start ()
    {
        src = gameObject.GetComponent<AudioSource>();
        gc = GameObject.Find("Game Controller").GetComponent<GameController>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!gc.allContraintsTouched)
            gameObject.GetComponent<AudioSource>().volume = 0;
	}

    void ManageCollision(GameObject other, float value)
    {        
        // Change volume when touched by a selected joint
        if (other.GetComponent<BodyJoint>() != null)
            if (gc.selectedJoints.Contains(other.GetComponent<BodyJoint>()))
            {
                Debug.Log("Touched " + name);
                src.volume = value;

                if (restart)
                {
                    src.Stop();
                    src.Play();
                }
            }
    }

    void OnCollisionExit2D(Collision2D other)
    {
        ManageCollision(other.gameObject, 0);
    }

    void OnCollisionExit(Collision other)
    {
        ManageCollision(other.gameObject, 0);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        // Check if every constrain is being touched
        if (!gc.allContraintsTouched)
            return;

        ManageCollision(other.gameObject, 1);
    }

    void OnCollisionEnter(Collision other)
    {
        // Check if every constrain is being touched
        if (!gc.allContraintsTouched)
            return;

        ManageCollision(other.gameObject, 1);
    }

    IEnumerator changeVolume()
    {


        yield return null;
    }
}
