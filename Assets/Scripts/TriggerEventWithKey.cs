﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerEventWithKey : MonoBehaviour
{
    [System.Serializable]
    public class TriggerEventWithKeyTarget
    {
        public KeyCode key = KeyCode.Space;
        public UnityEvent targetEvent;
    }
    public TriggerEventWithKeyTarget[] targets;

    // Update is called once per frame
    void Update()
    {
        foreach (TriggerEventWithKeyTarget target in targets)
            if (Input.GetKeyDown(target.key))
                target.targetEvent.Invoke();
    }
}
