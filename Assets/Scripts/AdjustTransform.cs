﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdjustTransform : MonoBehaviour
{
    public float zoomSpeed;
    Vector3 startPos;
    Vector3 startScale;
    // Use this for initialization
    void Start()
    {
        startPos = transform.position;
        startScale = transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.R))
        {
            transform.position = startPos;
            transform.localScale = startScale;
        }

        if (Input.GetKey(KeyCode.Q))
        {
            transform.localScale = new Vector3(transform.localScale.x * zoomSpeed, transform.localScale.y * zoomSpeed, transform.localScale.z);
        }
        if (Input.GetKey(KeyCode.E))
        {
            transform.localScale = new Vector3(transform.localScale.x / zoomSpeed, transform.localScale.y / zoomSpeed, transform.localScale.z); ;
        }

        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector2.left / 20);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector2.right / 20);
        }
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector2.up / 20);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector2.down / 20);
        }
    }
}
