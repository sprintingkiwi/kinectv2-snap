﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kinect = Windows.Kinect;

public class BodyJoint : MonoBehaviour
{
    public Kinect.JointType jointType;
    public bool selected;
    GameController gc;

    // Use this for initialization
    public void Setup()
    {
        gc = GameObject.Find("Game Controller").GetComponent<GameController>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnMouseDown()
    {        
        if (!gc.selectedJoints.Contains(this))
            SelectJoint();
        else
            DeselectJoint();
    }

    public void SelectJoint()
    {
        if (gc.selectedJoints.Contains(this))
            return;

        gc.selectedJoints.Add(this);

        // Change color
        if (gameObject.GetComponent<SpriteRenderer>() != null)
            gameObject.GetComponent<SpriteRenderer>().color = new Color(255, 0, 0);
        if (gameObject.GetComponent<MeshRenderer>() != null)
            gameObject.GetComponent<MeshRenderer>().material.color = new Color(255, 0, 0);

        gameObject.layer = 0;
    }

    public void DeselectJoint()
    {
        gc.selectedJoints.Remove(this);
        if (gameObject.GetComponent<SpriteRenderer>() != null)
            gameObject.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255);
        gameObject.layer = 8;
    }
}
