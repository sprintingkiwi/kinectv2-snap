﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContactTime : MonoBehaviour
{

    public Text showText;

    void Start()
    {
        showText.text = gameObject.GetComponent<Slider>().value.ToString();
    }

    public void UpdateText()
    {
        showText.text = gameObject.GetComponent<Slider>().value.ToString();
    }
}
