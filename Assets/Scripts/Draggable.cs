﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Draggable : MonoBehaviour
{
    public bool dragged;
    public Vector3 startPos;
    public bool destroyWhenPutAway;

    Vector3 dist;
    float posX;
    float posZ;
    float posY;

    // Use this for initialization
    void Start()
    {
        startPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (dragged)
        {
            //Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //transform.position = new Vector3(mousePos.x, mousePos.y, mousePos.z);
        }
    }

    void OnMouseDrag()
    {
        float disX = Input.mousePosition.x - posX;
        float disY = Input.mousePosition.y - posY;
        float disZ = Input.mousePosition.z - posZ;
        Vector3 lastPos = Camera.main.ScreenToWorldPoint(new Vector3(disX, disY, disZ));
        transform.position = new Vector3(lastPos.x, lastPos.y, lastPos.z);
    }

    void OnMouseDown()
    {
        dragged = true;
        gameObject.layer = 8;

        startPos = transform.position;
        dist = Camera.main.WorldToScreenPoint(transform.position);
        posX = Input.mousePosition.x - dist.x;
        posY = Input.mousePosition.y - dist.y;
        posZ = Input.mousePosition.z - dist.z;
    }

    void OnMouseUp()
    {
        dragged = false;
        gameObject.layer = 0;
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.name.Contains("Panel") && !dragged)
        {
            Debug.Log(name + " returning to start position");
            if (destroyWhenPutAway)
                Destroy(gameObject);
            else
            {
                dragged = false;
                transform.position = startPos;
            }          
        }
    }
}
