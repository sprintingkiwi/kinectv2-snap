﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloneGenerator : MonoBehaviour
{
    public GameObject source;
    public Transform cloneStartPosition;
    public Transform parent;
    public float deltaPos;

    public void Clone()
    {
        GameObject clone = Instantiate(source, cloneStartPosition.position, Quaternion.identity, parent);
        clone.transform.Translate(new Vector3(Random.Range(0, deltaPos), Random.Range(0, deltaPos), 0));
    }
}
