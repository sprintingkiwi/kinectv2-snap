﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCostumes : MonoBehaviour
{
    public Sprite[] costumes;
    int index;

    // Use this for initialization
    void Start()
    {
        index = 0;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Change()
    {
        if (index < costumes.Length - 1)
            index += 1;
        else
            index = 0;

        gameObject.GetComponent<SpriteRenderer>().sprite = costumes[index];

        // Reset paths colliders to fit new costume
        Destroy(gameObject.GetComponent<PolygonCollider2D>());
        gameObject.AddComponent<PolygonCollider2D>();
        //Destroy(gameObject.GetComponent<BoxCollider2D>());
        //gameObject.AddComponent<BoxCollider2D>().isTrigger = true;
    }
}
