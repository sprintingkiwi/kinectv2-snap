﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constraint : MonoBehaviour
{
    public bool touched;
    GameController gc;

    // Use this for initialization
    void Start()
    {
        gc = GameObject.Find("Game Controller").GetComponent<GameController>();
        gc.constraints.Add(this);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionExit2D(Collision2D other)
    {
        ManageCollision(other.gameObject, false);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        ManageCollision(other.gameObject, true);
    }

    void ManageCollision(GameObject other, bool value)
    {
        if (other.GetComponent<BodyJoint>() != null)
            if (gc.selectedJoints.Contains(other.GetComponent<BodyJoint>()))
            {
                Debug.Log("Touched " + name + " : " + value.ToString());
                touched = value;
            }
    }

    void OnDestroy()
    {
        gc.constraints.Remove(this);
    }
}
