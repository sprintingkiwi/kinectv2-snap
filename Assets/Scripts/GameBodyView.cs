﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Kinect = Windows.Kinect;

public class GameBodyView : BodySourceView {

    public bool Skeleton2D = false;
    public float distance = 0;
    public List<Kinect.JointType> hiddenJoints = new List<Kinect.JointType>();
    public List<Kinect.JointType> defaultSelected = new List<Kinect.JointType>();
    GameController gc;

    public override void Start()
    {
        base.Start();

        gc = GameObject.Find("Game Controller").GetComponent<GameController>();
    }

    public new Dictionary<Kinect.JointType, Kinect.JointType> _BoneMap = new Dictionary<Kinect.JointType, Kinect.JointType>()
    {
        { Kinect.JointType.FootLeft, Kinect.JointType.AnkleLeft },
        { Kinect.JointType.AnkleLeft, Kinect.JointType.KneeLeft },
        { Kinect.JointType.KneeLeft, Kinect.JointType.HipLeft },
        { Kinect.JointType.HipLeft, Kinect.JointType.SpineBase },

        { Kinect.JointType.FootRight, Kinect.JointType.AnkleRight },
        { Kinect.JointType.AnkleRight, Kinect.JointType.KneeRight },
        { Kinect.JointType.KneeRight, Kinect.JointType.HipRight },
        { Kinect.JointType.HipRight, Kinect.JointType.SpineBase },

        { Kinect.JointType.HandTipLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.ThumbLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.HandLeft, Kinect.JointType.ElbowLeft },
        { Kinect.JointType.ElbowLeft, Kinect.JointType.ShoulderLeft },
        { Kinect.JointType.ShoulderLeft, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.HandTipRight, Kinect.JointType.HandRight },
        { Kinect.JointType.ThumbRight, Kinect.JointType.HandRight },
        { Kinect.JointType.HandRight, Kinect.JointType.ElbowRight },
        { Kinect.JointType.ElbowRight, Kinect.JointType.ShoulderRight },
        { Kinect.JointType.ShoulderRight, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.SpineBase, Kinect.JointType.SpineMid },
        { Kinect.JointType.SpineMid, Kinect.JointType.SpineShoulder },
        { Kinect.JointType.SpineShoulder, Kinect.JointType.Neck },
        { Kinect.JointType.Neck, Kinect.JointType.Head },
    };

    public override GameObject CreateBodyObject(ulong id)
    {
        GameObject body = new GameObject("Body:" + id);
        body.transform.parent = GameObject.Find("BODIES").transform;
        body.AddComponent<TrackedBody>();
        Instantiate(Resources.Load("Selector") as GameObject, body.transform);

        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            if (hiddenJoints.Contains(jt))
                continue;

            // Determine joint resource name to load
            string jointToLoad = "Joint";
            string target = jt.ToString();
            if (Skeleton2D)
            {
                jointToLoad += "2D";
                target += "2D";
            }
            if (Resources.Load("CustomJoints/" + target) != null)
                jointToLoad = "CustomJoints/" + target;
            
            Debug.Log("Loading " + jointToLoad);

            // Instantiate joint
            GameObject jointObj = Instantiate(Resources.Load(jointToLoad) as GameObject);
            BodyJoint bj = jointObj.GetComponent<BodyJoint>();
            bj.Setup();
            bj.jointType = jt;
            if (gc.selectedJointTypes.Contains(jt))
                bj.SelectJoint();

            // Auto-select default selected joints
            if (defaultSelected.Contains(jt))
                bj.SelectJoint();

            // Bones line rendering
            LineRenderer lr = jointObj.GetComponent<LineRenderer>();
            lr.SetVertexCount(2);
            lr.material = BoneMaterial;
            //lr.SetWidth(0.05f, 0.05f);

            //jointObj.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            jointObj.name = jt.ToString();
            jointObj.transform.parent = body.transform;
        }

        // Re-Toggle RGB if RGB is ON
        if (gc.RGB)
            gc.ToggleRGB(true);

        return body;
    }

    public override void RefreshBodyObject(Kinect.Body body, GameObject bodyObject)
    {
        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            if (hiddenJoints.Contains(jt))
                continue;

            Kinect.Joint sourceJoint = body.Joints[jt];
            Kinect.Joint? targetJoint = null;

            if (_BoneMap.ContainsKey(jt))
            {
                targetJoint = body.Joints[_BoneMap[jt]];
            }

            Transform jointObj = bodyObject.transform.Find(jt.ToString());
            jointObj.localPosition = GetVector3FromJoint(sourceJoint);


            if (Skeleton2D == false)
            {
                distance = jointObj.transform.position.z;
            }
            jointObj.transform.position = new Vector3(
                                            jointObj.transform.position.x, 
                                            jointObj.transform.position.y, 
                                            distance);

            if (Skeleton2D)
            {
                LineRenderer lr = jointObj.GetComponent<LineRenderer>();
                if (targetJoint.HasValue)
                {
                    lr.SetPosition(0, new Vector3(jointObj.transform.position.x, jointObj.transform.position.y, distance));
                    lr.SetPosition(1, new Vector3 (GetVector3FromJoint(targetJoint.Value).x, GetVector3FromJoint(targetJoint.Value).y, distance));
                    lr.SetColors(GetColorForState(sourceJoint.TrackingState), GetColorForState(targetJoint.Value.TrackingState));
                }
                else
                {
                    lr.enabled = false;
                }
            }
            else
            {
                LineRenderer lr = jointObj.GetComponent<LineRenderer>();
                if (targetJoint.HasValue)
                {
                    lr.SetPosition(0, jointObj.localPosition);
                    lr.SetPosition(1, GetVector3FromJoint(targetJoint.Value));
                    lr.SetColors(GetColorForState(sourceJoint.TrackingState), GetColorForState(targetJoint.Value.TrackingState));
                }
                else
                {
                    lr.enabled = false;
                }
            }
            

            // Update dictionary for Snap! link
            if (body.TrackingId == snapBody)
                jointsDict[jt.ToString()] = jointObj.position;
        }
    }
}
