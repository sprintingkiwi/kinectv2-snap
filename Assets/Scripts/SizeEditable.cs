﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SizeEditable : MonoBehaviour
{
    public float rotationSpeed;
    public float sizeSpeed;
    public float sizeMin;
    public float sizeMax;
    public bool mouseOver;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (mouseOver)
        {
            // Rotation
            if (Input.GetKey(KeyCode.RightArrow))
                transform.Rotate(Vector3.forward * rotationSpeed);
            if (Input.GetKey(KeyCode.LeftArrow))
                transform.Rotate(Vector3.back * rotationSpeed);

            // Size
            if (Input.GetKeyDown(KeyCode.UpArrow))
                if (transform.localScale.x < sizeMax)
                    transform.localScale += Vector3.one * sizeSpeed;
            if (Input.GetKeyDown(KeyCode.DownArrow))
                if (transform.localScale.x > sizeMin)
                    transform.localScale -= Vector3.one * sizeSpeed;
        }
    }

    void OnMouseEnter()
    {
        mouseOver = true;
    }

    void OnMouseExit()
    {
        mouseOver = false;
    }
}
