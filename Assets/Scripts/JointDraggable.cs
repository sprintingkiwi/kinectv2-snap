﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JointDraggable : MonoBehaviour
{
    GameController gc;
    public bool dragged;
    public BodyJoint draggingJoint;
    public GameObject target;
    public float minDistanceToFollow;
    public AudioClip sound;

    // Use this for initialization
    void Start()
    {
        gc = GameObject.Find("Game Controller").GetComponent<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (dragged)
            if (draggingJoint != null)
                if (Vector3.Distance(transform.position, draggingJoint.transform.position) > minDistanceToFollow)
                    transform.position = draggingJoint.transform.position;
    }

    //void OnCollisionExit2D(Collision2D other)
    //{
    //    ManageCollision(other.gameObject, 0);
    //}

    //void OnCollisionExit(Collision other)
    //{
    //    ManageCollision(other.gameObject, 0);
    //}

    void OnCollisionEnter2D(Collision2D other)
    {
        // Check if every constrain is being touched
        if (!gc.allContraintsTouched)
            return;

        ManageCollision(other.gameObject, 1);
    }

    void OnCollisionEnter(Collision other)
    {
        // Check if every constrain is being touched
        if (!gc.allContraintsTouched)
            return;

        ManageCollision(other.gameObject, 1);
    }

    void ManageCollision(GameObject other, float value)
    {
        // Change volume when touched by a selected joint
        if (other.GetComponent<BodyJoint>() != null)
            if (gc.selectedJoints.Contains(other.GetComponent<BodyJoint>()))
            {
                Debug.Log("Touched " + name);

                if (value == 1)
                {
                    dragged = true;
                    draggingJoint = other.GetComponent<BodyJoint>();
                }
            }

        // Target reached
        if (other == target)
        {
            Utils.PlaySound(sound);
            Utils.ChangeScore(1);
            Destroy(gameObject);
        }
    }

    //IEnumerator ReachTarget()
    //{
    //    float step = 10 * Time.deltaTime; // calculate distance to move
    //    transform.position = Vector3.MoveTowards(transform.position, target.position, step);

    //    yield return null;
    //}
}
