﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Kinect = Windows.Kinect;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{   
    public List<BodyJoint> selectedJoints = new List<BodyJoint>();
    public List<Kinect.JointType> selectedJointTypes = new List<Kinect.JointType>();
    public List<Constraint> constraints = new List<Constraint>();
    public bool allContraintsTouched;
    public GameObject bodyTemplate;
    public Sprite[] jointCostumes;
    public bool RGB;
    public bool bones;
    public GameObject bodyManager2DPrefab;
    GameObject bm2DInstance;
    public GameObject bodyManager3DPrefab;
    GameObject bm3DInstance;
    public bool trackingStarted;
    public bool is2D;
    public Text scoreText;
    public int score;
    public void ResetScore()
    {
        score = 0;
    }
    public void ResetConstraints()
    {
        constraints.Clear();
    }

    // Use this for initialization
    void Start()
    {
        bodyTemplate = Instantiate(bodyTemplate);
        foreach(Transform t in bodyTemplate.transform)
        {
            t.GetComponent<BodyJoint>().Setup();
        }

        Utils.Log("Select joints", 10);

        bones = true;
    }

    // Update is called once per frame
    void Update()
    {
        // Constraints
        allContraintsTouched = true;
        foreach (Constraint c in constraints)
            if (!c.touched)
                allContraintsTouched = false;

        // Bones
        if (Input.GetKeyDown(KeyCode.B))
            ToggleBones(!bones);
    }

    public void Shutdown()
    {
        Application.Quit();
    }

    public void StartTracking()
    {
        foreach (BodyJoint bj in selectedJoints.ToArray())
        {
            selectedJointTypes.Add(bj.jointType);
            selectedJoints.Remove(bj);
        }

        Destroy(bodyTemplate, 0.5f);

        if (is2D)
            bm2DInstance = Instantiate(bodyManager2DPrefab) as GameObject;
        else
            bm3DInstance = Instantiate(bodyManager3DPrefab) as GameObject;

        StartCoroutine(Tracking());

        trackingStarted = true;
    }

    IEnumerator Tracking()
    {
        yield return new WaitForSeconds(1f);



        yield return null;
    }

    public void ToggleBones(bool status)
    {
        bones = status;

        if (status)
        {
            foreach (Transform body in GameObject.Find("BODIES").transform)
            {
                foreach (Transform joint in body)
                {
                    if (joint.name == "Selector")
                        continue;

                    if (joint.GetComponent<LineRenderer>() != null)
                    {
                        joint.GetComponent<LineRenderer>().startWidth = 0.3f;
                        joint.GetComponent<LineRenderer>().startWidth = 0.3f;
                        //joint.GetComponent<LineRenderer>().startColor = Color.clear;
                        //joint.GetComponent<LineRenderer>().endColor = Color.clear;
                    }
                }
            }
        }
        else
        {
            foreach (Transform body in GameObject.Find("BODIES").transform)
            {
                foreach (Transform joint in body)
                {
                    if (joint.name == "Selector")
                        continue;

                    if (joint.GetComponent<LineRenderer>() != null)
                    {
                        joint.GetComponent<LineRenderer>().startWidth = 0.01f;
                        joint.GetComponent<LineRenderer>().startWidth = 0.01f;
                        //joint.GetComponent<LineRenderer>().startColor = Color.white;
                        //joint.GetComponent<LineRenderer>().endColor = Color.white;
                    }
                }
            }
        }
    }

    public void ToggleRGB(bool status)
    {
        RGB = status;

        if (status)
        {
            foreach (Transform body in GameObject.Find("BODIES").transform)
            {
                foreach (Transform joint in body)
                {
                    if (joint.name == "Selector")
                        continue;

                    //joint.localScale = joint.localScale / 2;
                    if (joint.GetComponent<SpriteRenderer>() != null)
                    {
                        joint.GetComponent<SpriteRenderer>().enabled = false;
                        //joint.GetComponent<SpriteRenderer>().sprite = jointCostumes[1];
                    }
                }
            }
        }
        else
        {
            foreach (Transform body in GameObject.Find("BODIES").transform)
            {
                foreach (Transform joint in body)
                {
                    if (joint.name == "Selector")
                        continue;

                    //joint.localScale = joint.localScale * 2;
                    if (joint.GetComponent<SpriteRenderer>() != null)
                    {
                        joint.GetComponent<SpriteRenderer>().enabled = true;
                        //joint.GetComponent<SpriteRenderer>().sprite = jointCostumes[0];
                    }
                }
            }
        }
    }

    public void Toggle3D(bool status)
    {
        is2D = !status;

        if (!trackingStarted)
            return;        

        if (is2D)
        {
            Destroy(bm3DInstance);
            bm2DInstance = Instantiate(bodyManager2DPrefab) as GameObject;
        }
        else
        {
            Destroy(bm2DInstance);
            bm3DInstance = Instantiate(bodyManager3DPrefab) as GameObject;
        }

        foreach (Transform t in GameObject.Find("BODIES").transform)
            Destroy(t.gameObject);
    }
}
