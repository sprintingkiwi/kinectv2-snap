﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Kinect = Windows.Kinect;

public class Ball : MonoBehaviour
{
    public float speed;
    public float height;
    public Transform target;
    public GameController gc;
    public BodyJoint touchedJoint;
    public float contactTime;
    public Vector3 startPos;
    public float startTime;
    public AudioClip sound;

    // Use this for initialization
    void Start()
    {
        gc = GameObject.Find("Game Controller").GetComponent<GameController>();
        startPos = transform.position;
    }

    void OnDisable()
    {
        Reset();
    }

    // Update is called once per frame
    void Update()
    {
        contactTime = GameObject.Find("Contact Time").GetComponent<Slider>().value;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        // Check if every constrain is being touched
        if (!gc.allContraintsTouched)
            return;

        // Start ball movement if touched by a selected joint
        if (collision.gameObject.GetComponent<BodyJoint>() != null)
            if (gc.selectedJoints.Contains(collision.gameObject.GetComponent<BodyJoint>()))
            {
                touchedJoint = collision.gameObject.GetComponent<BodyJoint>();
                startTime = Time.time;
                startPos = transform.position;
                StartCoroutine(ReachTarget());
                Debug.Log(name + " collided with " + collision.gameObject.name);
            }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<BodyJoint>() != null)
            if (collision.gameObject.GetComponent<BodyJoint>() == touchedJoint)
                touchedJoint = null;
    }

    public virtual IEnumerator Trajectory()
    {
        yield return null;
    }

    IEnumerator ReachTarget()
    {
        Debug.Log("Started moving " + name);

        target = GameObject.Find("Target").transform;

        // Wait for some time of contact
        BodyJoint startJoint = touchedJoint;
        while (Time.time - startTime < contactTime)
        {
            // Interrupt coroutine if not touching the ball or the constraints
            if ((touchedJoint != startJoint) || !gc.allContraintsTouched)
                yield break;
            transform.position = startPos;
            yield return null;
        }

        //Process Trajectory
        yield return StartCoroutine(Trajectory());
        Utils.ChangeScore(1);

        Debug.Log("Finished moving " + name);

        yield return new WaitForSeconds(3f);
        Reset();
        yield return null;
    }

    void Reset()
    {
        transform.position = startPos;
        gameObject.layer = 0;
        gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;
        gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        gameObject.GetComponent<Rigidbody2D>().angularVelocity = 0;
    }
}
