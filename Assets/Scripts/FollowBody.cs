﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowBody : MonoBehaviour {

    public float distance;

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        if (GameObject.Find("SpineMid") != null)
            transform.position = new Vector3(transform.position.x, transform.position.y, GameObject.Find("SpineMid").transform.position.z - distance);
    }
}
