﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasketBall : Ball
{
    public override IEnumerator Trajectory()
    {
        // Parabolic movement towards basket
        float t = 0;
        while (t < 1)
        {
            transform.position = MathParabola.Parabola(startPos, target.position, height, t);
            t += 0.01f;
            yield return null;
        }

        //Play Sound
        Utils.PlaySound(sound);

        // Now fall down with physics
        gameObject.layer = 9;
        gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector3(Random.Range(0, 1000), Random.Range(0, 1000), Random.Range(0, 1000)));
        gameObject.GetComponent<Rigidbody2D>().AddTorque(1000f);
        gameObject.GetComponent<Rigidbody2D>().gravityScale = 1;

        yield return null;
    }
}
