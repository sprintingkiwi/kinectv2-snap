﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DoSomethingOnClick : MonoBehaviour
{
    public UnityEvent unityEvent;
    Vector3 lastPos;

    void OnMouseDown()
    {
        lastPos = transform.position;
    }

    void OnMouseUp()
    {
        // Check it was not dragged
        if (transform.position != lastPos)
            return;

        unityEvent.Invoke();
    }
}
