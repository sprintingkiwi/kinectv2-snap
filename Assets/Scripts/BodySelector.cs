﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodySelector : MonoBehaviour
{
    Vector3 localPos;
    public Vector3 deltaPos;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.position = transform.parent.Find("Head").position + deltaPos;
    }

    void OnMouseDown()
    {
        Transform body = transform.parent;
        if (!body.GetComponent<TrackedBody>().hidden)
        {
            // Hide
            foreach (Transform t in body)
            {
                if (t.gameObject.GetComponent<BodyJoint>() == null)
                    continue;

                if (t.gameObject.GetComponent<SpriteRenderer>() != null)
                    t.gameObject.GetComponent<SpriteRenderer>().enabled = false;

                if (t.gameObject.GetComponent<Collider2D>() != null)
                    t.gameObject.GetComponent<Collider2D>().enabled = false;

                if (t.gameObject.GetComponent<LineRenderer>() != null)
                    t.gameObject.GetComponent<LineRenderer>().enabled = false;
            }

            body.GetComponent<TrackedBody>().hidden = true;
        }
        else
        {
            // Show
            foreach (Transform t in body)
            {
                if (t.gameObject.GetComponent<BodyJoint>() == null)
                    continue;

                if (t.gameObject.GetComponent<SpriteRenderer>() != null)
                    t.gameObject.GetComponent<SpriteRenderer>().enabled = true;

                t.gameObject.GetComponent<Collider2D>().enabled = true;
                t.gameObject.GetComponent<LineRenderer>().enabled = true;
            }

            body.GetComponent<TrackedBody>().hidden = false;
        }


    }
}
