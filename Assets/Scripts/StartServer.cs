﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Net;
using System.Threading;
using System.Linq;
using System.Text;
using Kinect = Windows.Kinect;

public class StartServer : MonoBehaviour
{
    public WebServer ws;
    //Tracker tracker;
    public Kinect.JointType testJoint;

    [Header("System")]
    public BodySourceView bodySource;
    Dictionary<string, Vector3> jointsDict;

    public void Start()
    {
        Debug.Log("ciao".Split(':'));

        ws = new WebServer(SendResponse, "http://localhost:8080/");
        ws.Run();
        Debug.Log("Started server on port 8080");

        //tracker = GameObject.Find("Main Camera").GetComponent<Tracker>();
        bodySource = GameObject.Find("BodyView").GetComponent<BodySourceView>();
        jointsDict = bodySource.jointsDict;
    }

    // Respond to HTTP request
    public string SendResponse(HttpListenerRequest request)
    {
        string path = request.Url.LocalPath;
        //Debug.Log("Path: " + path);

        switch (path)
        {
            case "/test":
                return "Hello World";

            case "/getpos":
                string requestedJoint = request.QueryString["joint"];
                string requestedAxis = request.QueryString["axis"];
                //Debug.Log("Joint: " + requestedJoint);
                //Debug.Log("Axis: " + requestedAxis);
                Vector3 j = jointsDict[requestedJoint];
                string r = "";
                if (requestedAxis == "x")
                    r = j.x.ToString();
                else if (requestedAxis == "y")
                    r = j.y.ToString();
                else if (requestedAxis == "z")
                    r = j.z.ToString();
                else
                    r = "Invalid axis";
                return r;

            case "/getdist":
                string jointA = request.QueryString["jointa"];
                string jointB = request.QueryString["jointb"];
                Vector3 ja = jointsDict[jointA];
                Vector3 jb = jointsDict[jointB];
                float distance = Vector3.Distance(ja, jb);
                return distance.ToString();

            case "/getangle":
                string pointA = request.QueryString["jointa"];
                string pointB = request.QueryString["jointb"];
                string pointC = request.QueryString["jointc"];
                Vector3 pa = jointsDict[pointA];
                Vector3 pb = jointsDict[pointB];
                Vector3 pc = jointsDict[pointC];
                //float angle = Vector2.Angle(pa - pb, pc - pb);
                float angle = ThreePointsAngle(pa, pb, pc);
                //Debug.Log(angle);
                return angle.ToString();

            default:
                return "<HTML><BODY>Welcome to ASPHI Snap extension webserver for Kinect!<br>{0}</BODY></HTML>";
        }
    }

    public float ThreePointsAngle(Vector3 a, Vector3 b, Vector3 c)
    {
        float d1 = Vector3.Distance(a, b);
        float d2 = Vector3.Distance(b, c);
        float d3 = Vector3.Distance(c, a);
        return Mathf.Acos(((Mathf.Pow(d1, 2) + Mathf.Pow(d2, 2) - Mathf.Pow(d3, 2)) / (2 * d1 * d2)));
    }
}
