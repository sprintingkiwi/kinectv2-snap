﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class Utils{

    public static void Log(string text, float lifeTime = 0)
    {
        if (GameObject.Find("Canvas") == null || GameObject.Find("LOGS") == null)
        {
            Debug.LogWarning("Missing Canvas or LOGS container");
            return;
        }

        Debug.Log(text);
        Text t = Object.Instantiate(Resources.Load("Log") as GameObject, GameObject.Find("Canvas").transform.Find("LOGS")).GetComponent<Text>();
        t.text = text;
        if (lifeTime > 0)
            GameObject.Destroy(t.gameObject, lifeTime);
    }

    public static void CleanLogs()
    {
        if (GameObject.Find("Canvas") == null || GameObject.Find("LOGS") == null)
        {
            Debug.LogWarning("Missing Canvas or LOGS container");
            return;
        }

        foreach (Transform g in GameObject.Find("LOGS").transform)
            GameObject.Destroy(g.gameObject);
    }

    public static void PlaySound(AudioClip audioClip)
    {
        AudioSource source = new GameObject(audioClip.name).AddComponent<AudioSource>();
        source.clip = audioClip;
        source.Play();
    }

    public static void ChangeScore(int value)
    {
        GameController gc = GameObject.Find("Game Controller").GetComponent<GameController>();
        gc.score += value;
        gc.scoreText.text = gc.score.ToString();
    }

    public static void SetScore(int value)
    {
        GameController gc = GameObject.Find("Game Controller").GetComponent<GameController>();
        gc.score = value;
        gc.scoreText.text = gc.score.ToString();
    }

}
